import ApiService from '../../utils/api/api-service';
import ApiResult from '../../utils/api/models/api-result';
import {  Review_I_Req, Review} from '../../models/review'


class ReviewService extends ApiService {
  constructor() {
    super({ baseURL: `${process.env.NEXT_PUBLIC_PATH}api/proxy/api` });
  }

  public Insert = async (
    review: Review_I_Req
  ): Promise<ApiResult<Review>> =>
    this.post<Review>(`/add-rating-and-review`, review);
}

export const reviewService = new ReviewService();
