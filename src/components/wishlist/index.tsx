// ----------------------- React Imports---------------------------
import React, { useEffect, useState } from "react";
// ----------------------- UI & Style Imports---------------------------
import { Row, Col, Typography, notification, Rate, Button } from "antd";

const { Text } = Typography;

import { useDispatch, useSelector } from "react-redux";
import {
  DeleteItemItem,
  selectIsMobile,
  UpdateItem,
  InsertItem,
} from "../../redux";

// ----------------------- Other Imports -----------------------
import useTranslation from "next-translate/useTranslation";
// ----------------------- Interfaces -----------------------
import { Product, User } from "../../models";
import { responsive_constant, primaryColor } from "../../constants/layout";
import { BText } from "../../constants/shared/b-text";
import { BButton } from "../../constants/shared/b-button";
import { ShoppingCartOutlined, DeleteOutlined } from "@ant-design/icons";

interface propsInterface {
  cart: { product: Product }[];
  user: User | undefined;
}

const index: React.FC<propsInterface> = ({ cart, user }) => {
  const { t } = useTranslation("common");
  const isMobile = useSelector(selectIsMobile);
  // ------------------------Redux-------------------------------
  const dispatch = useDispatch();

  const cartNotification = (msg: string, description: string, type: string) => {
    notification[type === "success" ? "success" : "warning"]({
      message: msg,
      description: description,
      placement: "bottomRight",
      duration: 3,
    });
  };
  const handleAddtoCart = (product: Product) => {
    cartNotification(t`added-to-cart`, t`added-to-cart-desc`, "success");
    dispatch(InsertItem({ product, quantity: 1 }));
  };
  const handleDelete = (id: number) => {
    dispatch(DeleteItemItem(id));
  };

  return (
    <>
      <Row justify="center" style={{ marginBottom: "30px" }}>
        {cart.length > 0 ? (
          cart.map(({ product }) => (
            <Col
              {...responsive_constant}
              style={{
                textAlign: "center",
                fontSize: "0.9rem",
                fontWeight: "bold",
              }}
            >
              <Row
                justify="space-between"
                align="middle"
                style={{
                  borderBottom: "2px solid #dcdcdc",
                  marginBottom: "26px",
                  paddingBottom: "15px",
                }}
              >
                <Col span={4}>
                  <img
                    src={product.product_images[0].image_path}
                    style={{
                      width: "100%",
                      maxWidth: "100px",
                      border: "2px solid #aba8a8",
                      textAlign: "center",
                      borderRadius: 5,
                    }}
                  />
                </Col>

                <Col span={8}>
                  <Row style={{ padding: "10px" }}>
                    <Col span={24}>
                      <BText
                        lvl={5}
                        fw="light"
                        style={{ textTransform: "uppercase" }}
                      >
                        {product.brand?.name}
                      </BText>
                    </Col>
                    <Col span={24}>
                      <BText fw="bold">{product.name}</BText>
                    </Col>

                    <Col span={24}>
                      <Rate value={2} style={{ color: primaryColor }} />
                    </Col>
                    <Col span={24}>
                      <Row justify="space-around" align="middle" wrap={false}>
                        <Col>
                          <BText
                            color="gray"
                            style={{ textDecoration: "line-through 2px gray" }}
                          >
                            {product?.price}
                          </BText>
                        </Col>
                        <Col>
                          <BText> {product?.price_after_discount}</BText>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>

                <Col span={6}>
                  <BButton
                    style={{
                      backgroundColor: primaryColor,
                      padding: 8,
                      height: "100%",
                    }}
                    onClick={() => handleAddtoCart(product)}
                  >
                    <Row justify="space-around" align="middle" wrap={false}>
                      <Col>
                        <ShoppingCartOutlined
                          style={{ color: "#fff", marginRight: 10 }}
                        />
                      </Col>
                      <Col>
                        <Text style={{ color: "#fff" }}>{t`add-to-cart`}</Text>
                      </Col>
                    </Row>
                  </BButton>
                </Col>

                <Col span={6}>
                  <BButton
                    style={{
                      backgroundColor: primaryColor,
                      padding: 8,
                      height: "100%",
                    }}
                    onClick={() => handleDelete(product.id)}
                  >
                    <Row justify="space-around" align="middle" wrap={false}>
                      <Col>
                        <DeleteOutlined
                          style={{ color: "#fff", marginRight: 10 }}
                        />
                      </Col>
                      <Col>
                        <BText style={{ color: "#fff" }}>{t`delete`}</BText>
                      </Col>
                    </Row>
                  </BButton>
                </Col>
              </Row>
            </Col>
          ))
        ) : (
          <Row
            style={{
              textAlign: "center",
              fontSize: "3rem",
              height: "65vh",
            }}
            align="middle"
          >
            <Typography.Text
              style={{ fontSize: isMobile ? ".7em" : "1.2em" }}
              className="empty_cart"
            >
              {t`empty-wishlist`}
            </Typography.Text>
          </Row>
        )}
      </Row>
    </>
  );
};

export default index;
