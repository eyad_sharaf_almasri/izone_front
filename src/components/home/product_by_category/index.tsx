import { Row, Col } from "antd";
import useTranslation from "next-translate/useTranslation";
import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BText } from "../../../constants/shared/b-text";
import { Category } from "../../../models";
import {
  FetchProductsByCategoryAsync,
  selectProducts,
  selectStatusProducts,
} from "../../../redux";
import LoadingData from "../../LoadingData";
import ProductCard from "../../product-card";
import "./style.less";

interface Props {
  category?: Category;
}

export const HomeProductsByCategory: React.FC<Props> = ({ category }) => {
  const dispatch = useDispatch();
  const { lang } = useTranslation();
  const products_status = useSelector(selectStatusProducts);
  const productsbyCat = useSelector(selectProducts);

  useEffect(() => {
    category && dispatch(FetchProductsByCategoryAsync(category.id));
  }, [category, lang]);

  return (
    <LoadingData
      dataValid={() => (productsbyCat ? true : false)}
      loading={products_status === "loading"}
    >
      <Row gutter={[0, 40]}>
        <Col span={24}>
          <BText lvl={2}>{category?.name}</BText>
        </Col>

        <Col span={24}>
          <Row  gutter={[20, 20]} justify="space-between">
            {productsbyCat.map((el) => (
              <Col span={6}>
                <ProductCard key={el.id} product={el!} />
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </LoadingData>
  );
};
