import React, { FC, useEffect } from "react";
import { Col, Row, Image, Space } from "antd";
import useTranslation from "next-translate/useTranslation";
import { useDispatch, useSelector } from "react-redux";
import LoadingData from "../../LoadingData";
import Slider from "../../slider";
import Link from "next/link";
import {
  selectCategoriesStatus,
  selectCategories,
  FetchCategoriesAsync,
} from "../../../redux";
import "./style.less";
import { Category } from "../../../models";
import { BText } from "../../../constants/shared/b-text";
import ProductCard from "../../product-card";

interface Props {
  onChange?: (p: Category) => void;
}

export const Category_Slider: FC<Props> = ({ onChange }) => {
  const dispatch = useDispatch();
  const { lang } = useTranslation();
  const status = useSelector(selectCategoriesStatus);
  const categories = useSelector(selectCategories);

  useEffect(() => {
    dispatch(FetchCategoriesAsync());
  }, [lang]);

  useEffect(() => {
    categories && categories.length && onChange && onChange(categories[0]);
  }, [status]);

  return (
    <Row justify="space-around">
      <Col xl={24} lg={24} md={24} xs={20}>
        <LoadingData
          dataValid={() => (categories ? true : false)}
          loading={status === "loading"}
        >
          <Slider onChange={(ind) => onChange && onChange(categories[ind])}>
            {categories.map((category) => (
              <Space key={category.id} direction="horizontal">
                <div style={{ display: "flex", justifyContent: "center" }}>
                  <Image
                    className="category_img"
                    key={category.name}
                    src={category.image_path}
                    preview={false}
                  />
                </div>
                <div style={{ textAlign: "center" }}>
                  <BText color="gray">{category.name}</BText>
                </div>
              </Space>
            ))}
          </Slider>
        </LoadingData>
      </Col>
    </Row>
  );
};
