import React, { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row } from "antd";
import Slider from "../../slider";
import LoadingData from "../../LoadingData";
import useTranslation from "next-translate/useTranslation";
import events from "events";
import { Product } from "../../../models";
import {
  selectProductsStatus,
  selectProductsSalesAll,
  FetchTopSalesProductsAsync,
} from "../../../redux";
import TopSalesCard from "../../top-sales";

const TopSales: FC = () => {
  const { lang } = useTranslation();
  const dispatch = useDispatch();
  const status = useSelector(selectProductsStatus);
  const products = useSelector(selectProductsSalesAll);

  useEffect(() => {
    dispatch(FetchTopSalesProductsAsync());
  }, [lang]);
  // FetchTopSalesProductsAsync
  return (
    <Row justify="space-around">
      <Col xl={24} lg={24} md={24} xs={20}>
        <LoadingData
          dataValid={() => (products ? true : false)}
          loading={status === "loading"}
        >
          <Slider>
            {products?.map((product: Product) => (
              <TopSalesCard key={product.id} product={product!} />
            ))}
          </Slider>
        </LoadingData>
      </Col>
    </Row>
  );
};

export default TopSales;
