import React, { useEffect, useRef } from 'react';

const GMap = ({ branch }) => {
  const googleMapRef = useRef(null);
  let googleMap = null;

  useEffect(() => {
    // eslint-disable-next-line react-hooks/exhaustive-deps
    let cor = branch?.coordinates ? JSON.parse(branch.coordinates) : { lan: 33.50717877635181, lon: 36.276787279904106 };
    
    // log the coordinates

    googleMap = initGoogleMap(Number(cor.lan), Number(cor.lon));
    createMarker(Number(cor.lan), Number(cor.lon));
  }, [branch]);

  // initialize the google map
  const initGoogleMap = (lan, lon) => {
    return new window.google.maps.Map(googleMapRef.current, {
      center: { lat: 33.50717877635181, lng: 36.276787279904106  },
      zoom: 8,
    });
  };

  // create marker on google map
  const createMarker = (lan, lon) =>
    new window.google.maps.Marker({
      position: { lat: 33.50717877635181, lng: 36.276787279904106 },
      map: googleMap,
    });

  return <div ref={googleMapRef} style={{ minWidth: 300, height: 400 }} />;
};

export default GMap;
