import { Col, Row, Space, Typography } from "antd";
import React, { CSSProperties, FC } from "react";
import { Branch } from "../../models";
import {
  FacebookFilled,
  InstagramFilled,
  LinkedinFilled,
  PhoneFilled,
} from "@ant-design/icons";
import { Button, Input } from "antd";
import useTranslation from "next-translate/useTranslation";
import { TelegramFilled } from "../../constants/svgs/telegram-filled";
const { Text, Link, Title } = Typography;

const icon_layout: { style: CSSProperties } = {
  style: {
    padding: "15px",
    fontSize: "22px",
    borderRadius: "50%",
    backgroundColor: "#fff",
  },
};

const ContactInfo: FC = () => {
  const { t, lang } = useTranslation("common");
  const en = lang === "en";
  const ar = lang === "ar";
  return (
    <Row
      align="middle"
      justify="center"
      style={{
        paddingTop: "30px",
        width: "100%",
        height: "100%",
      }}
      gutter={[16, 16]}
    >
      <Col span={24} style={{ marginBottom: 30 }}>
        <Row align="middle" justify="space-between">
          <Col lg={24}>
            <Text style={{ fontWeight: "bold" }}>
              {t("contact-information")}
            </Text>
          </Col>
          {/* <Col lg={24}>
            <Space style={{ margin: '10px 0px' }} direction='horizontal' size='large'>
              <Col>
                <MailOutlined {...icon_layout} />
              </Col>
              <Col>
                <MobileOutlined {...icon_layout} />
              </Col>
              <Col>
                <FallOutlined {...icon_layout} />
              </Col>
            </Space>
          </Col> */}
        </Row>
      </Col>
      <Col lg={24}>
        <Row gutter={[16, 16]}>
          <Col span={24}>
            <Row>
              <Col span={6}>
                <PhoneFilled style={{ color: "#2aabe2" }} />
              </Col>
              <Col
                span={18}
                style={{
                  direction: "ltr",
                  justifyContent: ar ? "flex-end" : "flex-start",
                  display: "flex",
                }}
              >
                <Text
                  style={{
                    color: "#2a2a2a",
                    fontWeight: "bold",
                    direction: "ltr",
                  }}
                >
                  {"+971 55 838 9550"}
                </Text>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={6}>
                <TelegramFilled style={{ color: "#2aabe2" }} />
              </Col>
              <Col span={18}>
                <Text style={{ color: "#2a2a2a", fontWeight: "bold" }}>
                  {"sales@izone-me.com"}
                </Text>
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            <Row>
              <Col span={6}>
                {" "}
                <Text
                  style={{
                    color: "#2a2a2a",
                    fontWeight: "bold",
                    marginBottom: 3,
                  }}
                >
                  {t("follow-us")}
                </Text>
              </Col>
              <Col span={18}>
                <Space direction="horizontal" size="large">
                  <Link href="https://www.facebook.com/" target="_blank">
                    <Button
                      icon={<FacebookFilled style={{ color: "#2aabe2" }} />}
                      className="back"
                    />
                  </Link>
                  <Link href="https://www.instagram.com/" target="_blank">
                    <Button
                      icon={<InstagramFilled style={{ color: "#2aabe2" }} />}
                      className="back"
                    />
                  </Link>
                  <Link href="https://linkedin.com/" target="_blank">
                    <Button
                      icon={<LinkedinFilled style={{ color: "#2aabe2" }} />}
                      className="back"
                    />
                  </Link>
                  <Link href="https://telegram.com/" target="_blank">
                    <Button
                      icon={<TelegramFilled style={{ color: "#2aabe2" }} />}
                      className="back"
                    />
                  </Link>
                </Space>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
      <Col span={24}>
        <Row>
          <Col span={24}>
            <Text style={{ fontWeight: "bold" }}>
              {t`subscribe-to-newsletter`}
            </Text>
          </Col>
          <Col span={24}>
            <Row wrap={false} dir="ltr" style={{ direction: "ltr" }}>
              <Col flex="auto">
                <Input
                  size="large"
                  placeholder={t`enter-email-address`}
                  style={{
                    borderRadius: "5px 0 0 5px",
                    width: "100%",
                  }}
                />
              </Col>
              <Col flex="150px">
                <Button
                  size="large"
                  type="primary"
                  style={{ borderRadius: "0 5px 5px 0" }}
                  block
                >
                  {t`subscribe`}
                </Button>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};

export default ContactInfo;
