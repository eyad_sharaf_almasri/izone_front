import React, { useEffect } from "react";
import { Image, Col, Row, Typography, Space } from "antd";
import useTranslation from "next-translate/useTranslation";
import { useDispatch, useSelector } from "react-redux";
import {
  FetchBrandsAsync,
  selectBrands,
  selectBrandsStatus,
} from "../../redux/brand";
import LoadingData from "../LoadingData";
import { responsive_constant } from "../../constants/layout/responsive";

const { Title } = Typography;

// import "./style.less";
import Link from "next/link";

export default function Brands() {
  const dispatch = useDispatch();
  const { lang, t } = useTranslation("navbar");
  const status = useSelector(selectBrandsStatus);
  const brands = useSelector(selectBrands);

  useEffect(() => {
    dispatch(FetchBrandsAsync());
  }, [lang]);

  return (
    <LoadingData
      dataValid={() => (brands ? true : false)}
      loading={status === "loading"}
    >
      <Row style={{ padding: 10 }} gutter={[16, 16]}>
        <Col span={24}>
          <Title>{t`links.brands`}</Title>
        </Col>

        <Col span={24}>
          <Row gutter={[16, 16]}>
            {brands.map((brand) => (
              <Col
                span={6}
                sm={8}
                xs={12}
                style={{ display: "flex", justifyContent: "center" }}
              >
                <Space direction="vertical">
                  <Link href={`/brand/${brand.id}`}>
                    <Image
                      key={brand.id}
                      src={brand?.logo}
                      width={200}
                      height={200}
                      preview={false}
                      style={{ cursor: "pointer",objectFit:'contain' }}
                    />
                  </Link>
                  <Title level={4} style={{ textAlign: "center" }}>
                    {brand?.name}
                  </Title>
                </Space>
              </Col>
            ))}
          </Row>
        </Col>
      </Row>
    </LoadingData>
  );
}
