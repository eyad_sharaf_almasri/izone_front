import React, { CSSProperties, FC, useEffect, useState } from "react";
import {
  Image,
  Row,
  Typography,
  notification,
  Card,
  Col,
  Rate,
  Space,
} from "antd";
import {
  EyeFilled,
  HeartFilled,
  ShoppingCartOutlined,
} from "@ant-design/icons";
import { primaryColor } from "../../constants/layout/color";
import useTranslation from "next-translate/useTranslation";
import { Product } from "../../models/product";
import { useDispatch } from "react-redux";
import { InsertItem } from "../../redux";
import { InsertItemItem } from "../../redux";
import { selectUser } from "../../redux/app";
import { useSelector } from "react-redux";
import Link from "next/link";
import "./style.less";
import { BButton, BButtonProps } from "../../constants/shared/b-button";
import { BText, BTextProps } from "../../constants/shared/b-text";
import brand from "../../redux/brand";

interface product_props {
  product: Product;
}

const buttonLayout: BButtonProps = {
  color: "white",
  type: "primary",
  background: "secondary",
};
const textLayout: BTextProps = {
  color: "white",
  lvl: 5,
};

const ProductCard: FC<product_props> = ({ product }) => {
  const { t } = useTranslation("common");
  const dispatch = useDispatch();
  const [amount, setamount] = useState(1);
  const [active, setactive] = useState(false);
  const user = useSelector(selectUser);
  const cartNotification = (msg: string, description: string, type: string) => {
    notification[type === "success" ? "success" : "warning"]({
      message: msg,
      description: description,
      placement: "bottomRight",
      duration: 3,
    });
  };

  const onClick = () => {
    if (amount < 1) {
      cartNotification(
        t`error.added-to-cart-failed`,
        t`error.added-to-cart-failed-desc`,
        "fail"
      );
      ``;
    } else {
      cartNotification(t`added-to-cart`, t`added-to-cart-desc`, "success");
      dispatch(InsertItem({ product, quantity: amount }));
      setamount(1);
    }
  };
  const onClickk = () => {
    if (user && !active) {
      dispatch(InsertItemItem({ product }));
      setactive(true);
      cartNotification(
        t`added-to-wish-list`,
        t`added-to-wish-list-desc`,
        "success"
      );
    }
    if (active) {
      cartNotification(t`already-added`, t`I-can't-added-again`, "warning");
    }
    if (!user) {
      cartNotification(
        t`you-must-be-logged-in`,
        t`you-must-be-logged-in-desc`,
        "warning"
      );
    }
  };

  const [hover, setHover] = useState(false);

  return (
    <Card
      className="card-container"
      bodyStyle={{ padding: "10px" }}
      style={{
        width: '100%',
        marginRight: 10,
        marginLeft: 10,
        marginTop: 0,
        marginBottom: 10,
        minHeight: 370,
        height: 370
      }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      cover={
        <div style={{ position: "relative" }}>
          <Image
            preview={false}
            className="card-image"
            style={{ objectFit: "contain", width: "100%", height: 200,minHeight:200 }}
            src={product.product_images[0]?.thumbnail_250}
          />  

          <div
            style={{
              position: "absolute",
              right: 0,
              bottom: 10,
              transition: ".7s",
              opacity: hover ? 1 : 0,
            }}
          >
            <Space direction="vertical" size={3}>
              <Link href={`/products/${product.id}`}>
                <BButton
                  className="card_btn"
                  style={
                    {
                      "--i": 1,
                      WebkitTransform: hover
                        ? "perspective(0) rotateY(0deg)"
                        : "perspective(400) rotateY(90deg)",
                      transitionDelay: "0.1s",
                      background:'black',
                      border:'none'
                    } as React.CSSProperties
                  }
                  {...buttonLayout}
                >
                  <Row justify="space-between" align="middle" wrap={false}>
                    <Col>
                      <BText {...textLayout}>{t`view-product`}</BText>
                    </Col>
                    <Col>
                      <EyeFilled />
                    </Col>
                  </Row>
                </BButton>
              </Link>
              <BButton
                className="card_btn"
                style={
                  {
                    "--i": 2,
                    WebkitTransform: hover
                      ? "perspective(0) rotateY(0deg)"
                      : "perspective(400) rotateY(90deg)",
                    transitionDelay: "0.2s",
                    background:'black',
                    border:'none'
                  } as React.CSSProperties
                }
                {...buttonLayout}
                onClick={onClick}
              >
                <Row justify="space-between" align="middle" wrap={false}>
                  <Col>
                    <BText {...textLayout}>{t`add_to_card`}</BText>
                  </Col>
                  <Col>
                    <ShoppingCartOutlined />
                  </Col>
                </Row>
              </BButton>
              <BButton
                className="card_btn"
                style={
                  {
                    "--i": 3,
                    WebkitTransform: hover
                      ? "perspective(0) rotateY(0deg)"
                      : "perspective(400) rotateY(90deg)",
                    transitionDelay: "0.3s",
                    background:'black',
                    border:'none'
                  } as React.CSSProperties
                }
                {...buttonLayout}
              onClick={onClickk}
              >
                <Row justify="space-between" align="middle" wrap={false}>
                  <Col>
                    <BText{...textLayout}>{t`add-to-wish-list`}</BText>
                  </Col>
                  <Col>
                    <HeartFilled />
                  </Col>
                </Row>
              </BButton>
            </Space>
          </div>
        </div>
      }
      hoverable
    >
      <Row>
        <Col span={24}>
          <Rate
            value={product?.rating}
            style={{ color: primaryColor,fontSize:14 }}
            disabled
          />
        </Col>
        <Col span={24}>
          <BText lvl={4} fw="bold">{product.name}</BText>
        </Col>
        <Col span={24}>
          <BText lvl={5} fw="light" style={{ textTransform: "uppercase",color:'#888',marginTop:5 }}>
            {product.brand?.name}
          </BText>
        </Col>
        <Col span={24}>
          <Row justify="space-between" wrap={false}>
            <Col>
              <BText
                style={{ textDecoration: "line-through 2px #ccc", color:"#ccc"}}
                lvl={4}
              >
                {product?.price}
              </BText>
            </Col>
            <Col>
              <BText lvl={4}>{product?.price_after_discount}</BText>
            </Col>
            <Col>
              <BText lvl={4} style={{ padding: "0px 4px", border: "2px solid" }}>
                {product?.discount + "% Off"}
              </BText>
            </Col>
          </Row>
        </Col>
      </Row>
    </Card>
  );
};
export default ProductCard;
