import React, { FC, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  Col,
  Rate,
  Row,
  Typography,
  Image,
  Button,
  Modal,
  Form,
  Input,
  Radio,
} from "antd";
import { selectIsMobile } from "../../../redux";
import { Product } from "../../../models/product";
import useTranslation from "next-translate/useTranslation";
import { primaryColor } from "../../../constants/layout/color";
import { StarTwoTone } from "@ant-design/icons";
import { BButton } from "../../../constants/shared/b-button";
import { BText } from "../../../constants/shared/b-text";
import { InsertReviewAsync, selectProduct } from "../../../redux";
import { Review } from "../../../models/review";
import { count } from "console";

interface props {
  product: Product;
}

const { Title, Text } = Typography;

const iconStyle: React.CSSProperties = {
  fontSize: "2.5rem",
  marginRight: "15px",
};

const ReviewProducts: FC<props> = ({ product }) => {
  const isMobile = useSelector(selectIsMobile);

  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();

  const { t, lang } = useTranslation("single-product");

  const dispatch = useDispatch();
  // useEffect(() => {
  //   form.resetFields();
  // }, [lang]);

  useEffect(() => {
    console.log("review", product);
  }, []);

  const onFinish = (values: any) => {
    values.product_id = product?.id;
    dispatch(InsertReviewAsync(values));
    setVisible(false);
    form.resetFields();
  };
  return (
    <Row style={{ justifyContent: "space-between" }}>
      <Col xl={16} style={{ background: "white" }}>
        <Row justify="space-between">
          <Col>
            <BText fw="bolder">
              {t`reviews`}
              <BText>{product.reviews.length}</BText>
            </BText>
          </Col>
          <Col>
            <div>
              <BButton
                type="primary"
                style={{ width: 120, height: 50 }}
                onClick={() => {
                  setVisible(true);
                }}
              >{t`add-review`}</BButton>
              <Modal
                visible={visible}
                title="Create a new Review"
                onCancel={() => setVisible(false)}
                footer={null}
                destroyOnClose={true}
              >
                <Form
                  onFinish={onFinish}
                  form={form}
                  layout="vertical"
                  name="form_in_modal"
                  initialValues={{ modifier: "public" }}
                >
                  <Form.Item
                    name="rating"
                    rules={[{ required: true, message: t("reating") }]}
                  >
                    <Input
                      type="number"
                      min={1}
                      max={5}
                      placeholder={t("reating")}
                    />
                  </Form.Item>

                  <Form.Item
                    name="review_text"
                    rules={[{ required: true, message: t("summary") }]}
                  >
                    <Input.TextArea
                      autoSize={{ minRows: 6 }}
                      placeholder={t("summary")}
                    />
                  </Form.Item>
                  <Col span={24}>
                    <Row justify="center">
                      <Col>
                        <BButton size="large" type="primary" htmlType="submit">
                          {t`submit`}
                        </BButton>
                      </Col>
                    </Row>
                  </Col>
                </Form>
              </Modal>
            </div>
          </Col>
        </Row>
        <Row>
          <StarTwoTone style={{ fontSize: "30px" }} />
          <BText>
            <strong style={{ fontWeight: "bolder", fontSize: "25px" }}>
              {product.rating}
            </strong>
            /5
          </BText>
          <BText>{t`average-rating`}</BText>
        </Row>
        {product.reviews?.map((review) => (
          <div style={{ borderBottom: "1px solid", margin: "30px 0" }}>
            <Row>
              <Col
                style={{ display: "grid", marginLeft: "55px" }}
                key={review.id}
              >
                <Rate
                  style={{ color: primaryColor }}
                  disabled
                  value={review.rating}
                />
                <Text>{review.review_text}</Text>
              </Col>
            </Row>
          </div>
        ))}
      </Col>

      <Col xl={6} style={{ background: "white" }}>
        <Row>
          <BText fw="bolder">{t`top-reviews`}</BText>
        </Row>
        {product.top_reviews?.map((review) => (
          <div style={{ borderBottom: "1px solid", margin: "30px 0" }}>
            <Row>
              <Col
                style={{ display: "grid", marginLeft: "55px" }}
                key={review.id}
              >
                <div style={{ display: "grid", marginLeft: "-55px" }}>
                  <Rate
                    value={review.rating}
                    style={{ color: primaryColor }}
                    disabled
                  />
                  <Text>{review.review_text}</Text>
                </div>
              </Col>
            </Row>
          </div>
        ))}
      </Col>
    </Row>
  );
};
export default ReviewProducts;
