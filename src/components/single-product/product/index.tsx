import React, { FC, useState, CSSProperties } from "react";
import useTranslation from "next-translate/useTranslation";
import {
  FilePdfOutlined,
  HeartFilled,
  MinusCircleFilled,
  PlusCircleFilled,
  ShoppingCartOutlined,
} from "@ant-design/icons";
import {
  Button,
  Col,
  Row,
  Space,
  Typography,
  Image,
  notification,
  InputNumber,
} from "antd";
import { Product } from "../../../models/product";
import { primaryColor } from "../../../constants/layout/color";
import { useDispatch } from "react-redux";
import { InsertItem } from "../../../redux";

import "./style.less";
import Link from "next/link";
import { useWidth } from "../../../utils/helpers/use-is-mobile";
import { BButton } from "../../../constants/shared/b-button";
import { BText } from "../../../constants/shared/b-text";

const { Title, Text } = Typography;

const small_img_layout: {
  preview: boolean;
  height: number;
  style: CSSProperties;
} = {
  preview: false,
  height: 120,
  style: { objectFit: "cover", border: "1px solid #cfcfcf" },
};

const buy_btn_layout: {
  type: "primary";
  block: boolean;
  style: CSSProperties;
} = {
  type: "primary",
  block: true,
  style: {
    height: 60,
    width: "100%",
    display: "flex",
    borderRadius: 40,
    fontSize: "1.4em",
    alignItems: "center",
    justifyContent: "center",
  },
};

interface product_props {
  product: Product;
}

const ProductSection: FC<product_props> = ({ product }) => {
  const { t } = useTranslation("single-product");
  const dispatch = useDispatch();

  const [visible, setVisible] = useState(false);

  const { isMobile } = useWidth();

  const cartNotification = (msg: string, type: string) => {
    notification[type === "success" ? "success" : "warning"]({
      message: msg,
      placement: "bottomRight",
      duration: 3,
    });
  };

  const [amount, setamount] = useState(1);

  const handleBuy = () => {
    if (amount < 1) {
      cartNotification(t("common:error.added-to-cart-failed"), "failed");
    } else {
      setVisible(true);
      cartNotification(t("common:added-to-cart"), "success");
      dispatch(InsertItem({ product, quantity: amount }));
      setamount(1);
    }
  };
  return (
    <Row
      style={{
        padding: 30,
        justifyContent: "space-between",
        background: "white",
        marginTop: 64,
      }}
    >
      <Col lg={12} md={24} xs={24}>
        <Image
          style={{ ...small_img_layout.style, maxWidth: 400 }}
          width={"100%"}
          height={"100%"}
          preview={false}
          src={product.product_images[0]?.image_path}
        />
      </Col>
      <Col lg={12} md={24} xs={24}>
        <Row style={{ padding: 10 }}>
          <Col>
            <Space direction="vertical">
              <BText lvl={1}> {product.name}</BText>

              <Text style={{ fontSize: "1.2em", fontWeight: 500 }}>
                <Text style={{ fontWeight: 500, color: primaryColor }}>
                  {t("overview")}
                </Text>
                <br />
                <div dangerouslySetInnerHTML={{ __html: product.overview! }} />
              </Text>
            </Space>
          </Col>
        </Row>
        <Row style={{ padding: 10 }} justify="space-between">
          <Space direction={isMobile ? "vertical" : "horizontal"}>
            <BText
              color="gray"
              style={{ textDecoration: "line-through 2px gray" }}
            >
              SYR {product.price}
            </BText>
            <BText fw={"bolder"} color="primary" lvl={3}>
              SYR {product.price_after_discount}
            </BText>
          </Space>

          <BText
            style={{
              padding: "3px 8px",
              border: "2px solid",
              display: "end",
            }}
          >
            {product?.discount + "% Off"}
          </BText>
        </Row>

        <Row
          align="middle"
          justify="space-between"
          style={{ padding: 10 }}
          gutter={[16, 16]}
        >
          <Col lg={12} span={24}>
            <Space>
              <BButton
                onClick={() => {
                  setamount((amount) => (amount - 1 > 0 ? amount - 1 : 1));
                }}
              >
                -
              </BButton>
              <BButton
                type="primary"
                disabled
                style={{ backgroundColor: "#2aabe2", color: "#333" }}
              >
                {amount}
              </BButton>

              <BButton
                onClick={() => {
                  setamount((amount) => amount + 1);
                }}
              >
                +
              </BButton>
            </Space>
          </Col>
        </Row>

        <Row style={{ padding: 10 }}>
          <Col>
            <Space>
              <Button
                onClick={handleBuy}
                {...buy_btn_layout}
                style={{ marginTop: 30 }}
                icon={<ShoppingCartOutlined />}
              >
                {t("buy_product")}
              </Button>
              {visible && (
                <Link href="/cart">
                  <a>
                    <Button
                      {...buy_btn_layout}
                      style={{ marginTop: 30 }}
                      ghost
                      icon={<ShoppingCartOutlined />}
                    >
                      {t`go-to-cart`}
                    </Button>
                  </a>
                </Link>
              )}
            </Space>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default ProductSection;
