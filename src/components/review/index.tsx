import { Row, Col, Form, Input, Space } from "antd";
import useTranslation from "next-translate/useTranslation";
import React, { CSSProperties, FC } from "react";
import { useDispatch } from "react-redux";
import { largeShadow, styledInput } from "../../constants/layout";
import { BButton } from "../../constants/shared/b-button";
import { BText } from "../../constants/shared/b-text";
import { Product } from "../../models";
import { useWidth } from "../../utils/helpers/use-is-mobile";
import LoadingData from "../LoadingData";

const input_layout: { style: CSSProperties } = {
  style: { height: 50, borderRadius: 30 },
};

interface props {
  product: Product;
}
const AddReview: FC<props> = ({ product }) => {
  const { t } = useTranslation("review");
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { isMobile } = useWidth();

  const onFinish = () => {};

  return (
    // <LoadingData dataValid={() => (review ? true : false)} loading={status === 'loading'}>
    <Form form={form} onFinish={onFinish}>
      <Row justify="start" gutter={[0, 40]}>
        <Col
          span={24}
          style={{
            boxShadow: largeShadow,
            backgroundColor: "white",
            borderRadius: 20,
            margin: 10,
          }}
        >
          <Row
            gutter={[0, 36]}
            style={{
              padding: isMobile ? "30px" : "40px 120px",
            }}
          >
            <Col span={24}>
              <BText lvl={3} t={2} fw="bolder">
                {t`add-review`}
              </BText>
            </Col>
            <Col span={24}>
              <Row justify="space-between">
                <Col span={isMobile ? 24 : 10}>
                  <Form.Item
                    name="brand"
                    rules={[{ required: true, message: t("brand") }]}
                  >
                    <Input {...styledInput} placeholder={t("brand")} />
                  </Form.Item>
                </Col>
                <Col span={isMobile ? 24 : 10}>
                  <Form.Item
                    name="product"
                    rules={[{ required: true, message: t("product") }]}
                  >
                    <Input {...styledInput} placeholder={t("product")} />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <Row justify="center">
                <Col span={isMobile ? 24 : 10}>
                  <Form.Item
                    name="reating"
                    rules={[{ required: true, message: t("reating") }]}
                  >
                    <Input
                      type="number"
                      {...styledInput}
                      min={1}
                      max={5}
                      placeholder={t("reating")}
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col span="24">
              <Form.Item
                name="summary"
                rules={[{ required: true, message: t("summary") }]}
              >
                <Input.TextArea
                  {...styledInput}
                  autoSize={{ minRows: 6 }}
                  placeholder={t("summary")}
                />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Row justify="center">
                <Col>
                  <BButton size="large" type="primary" htmlType="submit">
                    {t`submit`}
                  </BButton>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Form>
    // </LoadingData>
  );
};
export default AddReview;
