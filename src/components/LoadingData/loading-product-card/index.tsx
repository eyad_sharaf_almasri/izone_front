import { Card, Col, Row, Skeleton } from "antd";
import React from "react";

interface Props {
  count?: number;
}

export const LoadingProductCard: React.FC<Props> = ({ count = 1 }) => {
  return (
    <Row justify="space-between" gutter={[24, 0]}>
      {[...Array(count).keys()].map((el) => (
        <Col key={el} span={6}>
          <Card>
            <Skeleton.Image style={{ width: "100%", height: 200 }} />
            <Skeleton active title />
            <Row justify="space-between">
              {[...Array(3).keys()].map((el) => (
                <Col key={el}>
                  <Skeleton.Button size="default" shape="round" active />
                </Col>
              ))}
            </Row>
          </Card>
        </Col>
      ))}
    </Row>
  );
};
