import { Empty, Skeleton, Spin } from "antd";
import React, { ReactElement } from "react";
import requestStatus from "../../../constants/enums/request-status";
import NetworkError from "../NetworkError";

type LoadingDataProps =
  | {
      loading: boolean;
      dataValid: boolean;
      isEmpty?: boolean;
      standing?: boolean;
      customError?: ReactElement;
      type?: "default" | "skeleton";
      customEmpty?: ReactElement;
      customStanding?: ReactElement;
    }
  | {
      status: requestStatus;
      isEmpty?: boolean;
      customError?: ReactElement;
      type?: "default" | "skeleton" | JSX.Element;
      customEmpty?: ReactElement;
      customStanding?: ReactElement;
    };

export const LoadingCategoryData: React.FC<LoadingDataProps> = (props) => {
  const {
    customError = <NetworkError />,
    type = "skeleton",
    customEmpty = <></>,
    // customEmpty = <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />,
    isEmpty = false,
    customStanding = <></>,
    children,
  } = props;

  let loadingStyle: JSX.Element;

  switch (type) {
    case "default":
      loadingStyle = <Spin />;
      break;

    case "skeleton":
      loadingStyle = <Skeleton active />;
      break;

    default:
      loadingStyle = type;
      break;
  }

  if ("status" in props) {
    switch (props.status) {
      case "no-thing":
        return customStanding;

      case "loading":
        return loadingStyle;

      case "error":
        return customError;

      case "data":
        if (isEmpty) return customEmpty;
        return <>{children}</>;

      default:
        return <>INIT</>;
    }
  } else {
    const { loading, children, dataValid } = props;

    return loading ? (
      loadingStyle
    ) : dataValid ? (
      isEmpty ? (
        customEmpty
      ) : (
        <React.Fragment>{children}</React.Fragment>
      )
    ) : (
      customError
    );
  }
};
