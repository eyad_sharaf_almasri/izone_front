import Icon from '@ant-design/icons';
import React from 'react';

const svg = () => <svg fill='currentColor' width='1em' height='1em' viewBox="0 0 19.454 18.358">
    <path d="M500.188,90.365,482.93,97.888a1.1,1.1,0,0,0-.065,1.981l3.5,1.806,10.426-7.252a.145.145,0,0,1,.229.116l-8.734,8.337a1.1,1.1,0,0,0-.34.794v3.861a1.1,1.1,0,0,0,2.034.572l2.119-3.467,4.484,2.314a1.1,1.1,0,0,0,1.57-.717l3.54-14.6A1.1,1.1,0,0,0,500.188,90.365Z" transform="translate(-482.271 -90.271)" />
</svg>

export const TelegramFilled: React.FC<{ style?: React.CSSProperties }> = ({ style }) => <Icon style={style} component={svg} />