import Icon from "@ant-design/icons";
import React from "react";

const svg = () => (
  <svg fill="currentColor" width="1em" height="1em" viewBox="0 0 43 43">
    <path
      d="M19.692,43.083A21.6,21.6,0,0,1,.083,23.484v-3.8a21.6,21.6,0,0,1,19.6-19.6h3.8a21.6,21.6,0,0,1,19.6,19.609v3.781A21.6,21.6,0,0,1,23.474,43.083ZM8.633,30.173a15.54,15.54,0,0,0,25.9,0c-.065-4.295-8.654-6.647-12.949-6.647C17.267,23.525,8.7,25.878,8.633,30.173Zm6.475-17.223a6.475,6.475,0,1,0,6.476-6.475A6.467,6.467,0,0,0,15.107,12.949Z"
      transform="translate(-0.083 -0.083)"
    />
  </svg>
);

export const UserFilled: React.FC<{ style?: React.CSSProperties }> = ({
  style,
}) => <Icon style={style} component={svg} />;
