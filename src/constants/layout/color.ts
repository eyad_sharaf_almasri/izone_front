export const primaryColor = '#2aabe2';
export const secondaryColor = '#333';
export const darkColor = '#000000d9';
export const lightColor = 'whitesmoke';
export const dangerColor = '#f5222d';
export const successColor = '#52c41a';
export const warningColor = '#faad14';
