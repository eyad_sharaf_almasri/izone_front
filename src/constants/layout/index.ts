import { primaryColor ,secondaryColor } from "./color";


export const linerBackground = `linear-gradient(to right, ${secondaryColor} , ${primaryColor})`;
export const smallShadow = '#0001 2px 1px 5px 1px';
export const middleShadow = '#0001 3px 3px 15px 3px';
export const largeShadow = '#0002 5px 5px 20px 4px';

export const smallGap = 12;
export const middleGap = 24;
export const largeGap = 36;

export * from './color';
export * from './responsive';
