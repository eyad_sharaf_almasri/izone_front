import React, { FC, useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "react-redux";
import { Col, Row, Image, Typography, Input, Button } from "antd";
import { Category, Product } from "../../models";
// import {selectUser} from "../../redux/app"
import {
  FetchBrandsAsync,
  FetchProductsByNameAsync,
  selectBrands,
  selectBrandsStatus,
  selectProducts, selectProductsStatus,
  selectCategories,
  FetchProductsByCategoryAsync,
} from "../../redux";
import LoadingData from "../../components/LoadingData";
import ProductCard from "../../components/product-card";
import useTranslation from "next-translate/useTranslation";
import { responsive_constant } from "../../constants/layout/responsive";

const { Title } = Typography;
interface props {
  category: Category;
}
const id: FC = () => {
  const { t } = useTranslation("common");
  const { lang } = useTranslation();
  const { query } = useRouter();
  const dispatch = useDispatch();
  const status = useSelector(selectBrandsStatus);
  // const categories= useSelector(selectBrands);
  const categories = useSelector(selectCategories);
  const [searched, setsearched] = useState<string>();
  // const user=useSelector(selectUser);
  const products_status = useSelector(selectProductsStatus);
  let products = useSelector(selectProducts);

  const { id } = query;

  const category = categories.find((category) => category.sub_categories?.find(el=>el.id === 100));

  // useEffect(() => {
  //   dispatch(FetchBrandsAsync());
  // }, [id, lang]);

  useEffect(() => {
    dispatch(FetchProductsByCategoryAsync(parseInt(id as string)));
  }, [lang]);

  //search product
  const handleSearch = (e: any) => {
    setsearched(e.target.value);
  };
  // useEffect(()=>{
  //   if(user?.missing_params)
  //   replace("/personal-collection");
  // },[user])

  let product_name = searched?.trim().toLowerCase();
  if (product_name && product_name.length > 0) {
   products = products.filter((val:any) => val?.name.toLowerCase().match(product_name!));
  }

  return (
    <Row justify="center">
      <Col {...responsive_constant} style={{ minHeight: 400, marginTop:'12px' }}>
       
          
          <Row  style={{ alignItems: "center",margin:16 }}>
            <Col sm={12}  xs ={24}> <Image src={category?.image_path} width={200} height={200} preview={false} /></Col>
            <Col sm={12} xs={24}> <Title>{category?.name}</Title></Col>
          </Row>

     
          <div className="input-container" style={{ margin: 16 }}>
            <Button
              className="send-email-btn"
              style={{
                width: "20%",
                top: 0,
                borderRadius: lang === "en" ? "22px 0 0 22px" : "",
              }}
              onClick={handleSearch}
              type="primary"
            >
              {t("search")}
            </Button>
            <Input
              placeholder={t("input_product_name")}
              className="send-email-input"
              style={{
                width: "100%",
                borderRadius: lang === "en" ? "0 22px 22px 0" : "",
              }}
              onChange={handleSearch}
              allowClear
            />
          </div>
          <LoadingData dataValid={() => (products ? true : false)} loading={products_status === "loading"}>
            <ProductsByCategories products={products} />
          </LoadingData>
        
      </Col>
    </Row>
  );
};
export default id;

export const ProductsByCategories: FC<{ products: Product[] }> = ({ products }) => {
  return (
    <Col>
      <Row gutter={[16,16]} justify='center'>
        {products.map((product) => (
          <Col lg={8} sm={12} xs={24}>
          <ProductCard product={product} />
          </Col>
        ))}
      </Row>
    </Col>
  );
};