import { Col, Row, Typography } from "antd";
import React, { FC } from "react";
import { primaryColor } from "../../constants/layout/color";

interface props {}
const { Text, Title } = Typography;

const RegisterSeller: FC = () => {
  return (
    <Row>
      <Col span={24} style={{ display: "flex", justifyContent: "center", alignItems: "center" }}>
        <Text style={{ fontSize: "4rem", color: primaryColor }}>COMMING SOON</Text>
      </Col>
    </Row>
  );
};

export default RegisterSeller;
