import { Button, Input, InputNumber, Form } from "antd";
import Checkbox from "antd/lib/checkbox/Checkbox";
import React from "react";
import useTranslation from "next-translate/useTranslation";
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

/* eslint-disable no-template-curly-in-string */

function CardForm() {
  const { t } = useTranslation("cart");

  const onFinish = (values: any) => {
    console.log(values);
  };
  return (
    <div style={{ marginTop: "50px" }}>
      <Form style={{ width: "60%", margin: "auto" }} name="nest-messages" onFinish={onFinish}>
        <h3>{t`card-information`}</h3>
        <div>
          <Form.Item rules={[{ required: true }]}>
            <Input placeholder="1234 1234 1234 1234" />
          </Form.Item>
          <Form.Item style={{ display: "flex", alignItems: "center" }} rules={[{ type: "email" }]}>
            <Input placeholder="MM/YY" style={{ width: "50%", flex: 1 }} />
            <Input placeholder="CVC" style={{ width: "50%", flex: 1 }} />
          </Form.Item>
        </div>

        <div>
          <h3>{t`name-of-card`}</h3>
          <Form.Item>
            <Input placeholder="Name of Card" />
          </Form.Item>
          <Form.Item valuePropName="checked">
            <Checkbox>
              {t`terms-accept`} <a href="">{t`terms-conditions`}</a>
            </Checkbox>
          </Form.Item>

          <Form.Item>
            <Button
              disabled
              style={{ width: "100%", background: "linear-gradient(to right, #8d2cd3 , #f93e4f", height: "50px", color: "#fff" }}
              type="primary"
              htmlType="submit"
            >
              {t`pay`}
            </Button>
          </Form.Item>
        </div>
      </Form>
    </div>
  );
}

export default CardForm;
