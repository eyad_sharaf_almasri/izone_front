import { Button, Input, Form } from "antd";
import Checkbox from "antd/lib/checkbox/Checkbox";
import React from "react";
import useTranslation from "next-translate/useTranslation";

function PayPalForm() {
  const { t } = useTranslation("cart");

  return (
    <div style={{ marginTop: "50px" }}>
      <Form style={{ width: "50%", margin: "auto" }} name="nest-messages">
        <h1 style={{ textAlign: "center" }}>{t`paypall`}</h1>

        <Form.Item rules={[{ required: true }]}>
          <Input placeholder={t`email`} />
        </Form.Item>
        <Form.Item>
          <Input type="password" placeholder={t`password`} />
        </Form.Item>

        <Form.Item valuePropName="checked">
          <Checkbox>
            {t`terms-accept`} <a href="">{t`terms-conditions`}</a>
          </Checkbox>
        </Form.Item>

        <Form.Item>
          <Button
            disabled
            style={{ width: "100%", background: "linear-gradient(to right, #8d2cd3 , #f93e4f", height: "50px", color: "#fff" }}
            type="primary"
            htmlType="submit"
          >
            {t`pay`}
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default PayPalForm;
