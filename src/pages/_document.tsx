import Document, { Html, Head, Main, NextScript } from 'next/document';

class CustomeDoc extends Document {
  render() {
    return (
      <Html lang='ar'>
        <Head>
          <link
            rel='preload'
            href='/fonts/RUBIK-REGULAR.TTF'
            as='font'
            type='font/TTF'
            crossOrigin='anonymous'
          />
        </Head>
        <body style={{ overflowX: 'hidden' }}>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default CustomeDoc;
