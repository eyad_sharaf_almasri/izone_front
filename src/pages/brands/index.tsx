import React, { FC, useEffect } from "react";
import { Image, Col, Row,Typography } from "antd";
import Brands from '../../components/brands'
import { responsive_constant } from "../../constants/layout/responsive";

const Braands: FC= () => {
  
  return (
    <Row
    justify="center"
    align="middle"
    style={{
      padding: "5px 0",
    }}
  >
   
    
    <Col {...responsive_constant}>
      <Row justify="space-between" align="middle">
    
     <Brands/>
     </Row>
    </Col>
  </Row>
  );
};

export default Braands;
