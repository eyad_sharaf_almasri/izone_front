// ---------------------React Imports---------------------
import { useEffect, useState } from 'react';
// ---------------------Redux Imports---------------------
import {
  selectWishListItem,
  selectUser,
  setOrderDetails,
  setOrderStatus,
} from '../redux';
import { useDispatch, useSelector } from 'react-redux';
// ---------------------Components Imports---------------------
import WishlistCarts from '../components/wishlist';


const wishlist: React.FC = () => {
  // ---------------------Redux & State---------------------
  const dispatch = useDispatch();
  const wishlist = useSelector(selectWishListItem);
  const user = useSelector(selectUser);


  useEffect(() => {
    // if user leaves the checkout process, rest it, but keep carts items

    // return () => {
    
    //   dispatch(setOrderStatus('no-thing'));
    //   dispatch(setOrderDetails(null));
    // };

  }, []);
 
        return (
          <WishlistCarts cart={wishlist} user={user} />
        );

};

export default wishlist;
