import React, { CSSProperties, FC } from 'react';
import { Col, Row, Typography } from 'antd';
import { responsive_constant } from '../../constants/layout/responsive';
import { primaryColor } from '../../constants/layout/color';
import useTranslation from 'next-translate/useTranslation';
import json_translate from '../../translations/terms-conditions/ar.json';

const { Title, Text } = Typography;

const title_layout: { style: CSSProperties } = {
  style: { color: primaryColor, fontSize: '1.6em' },
};

const subtitle_layout: { style: CSSProperties } = {
  style: { fontSize: '1.2em' },
};

const index: FC = () => {
  const { t } = useTranslation('terms-conditions');

  return (
    <Row
      justify='center'
      gutter={[0, 40]}
      style={{ padding: '75px 0', whiteSpace: 'pre-line' }}
    >
      <Col {...responsive_constant}>
        {/* main title */}
        <Title>{t('terms_conditions')}</Title>
        {/* main title */}
      </Col>
      <Col {...responsive_constant}>
      <Title {...title_layout}>{t('main_part.title')}</Title>
        <Text type='secondary' style={{ fontSize: '1.2em' }}>
                {t(`main_part.content`)}
         </Text>
      </Col>

     

      <Col {...responsive_constant}>
      <Title {...title_layout}>{t('part1.title')}</Title>
        <ul>
          {json_translate.part1.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
              <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                    {t(`part1.content.${i}.text`)}
                  </Text>
              </li>
              
            </Row>
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
      <Title {...title_layout}>{t('part2.title')}</Title>
        <ul>
          {json_translate.part2.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
               
                <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                    {t(`part2.content.${i}.text`)}
                  </Text>
                
              </li>
              
            </Row>
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
      <Title {...title_layout}>{t('part3.title')}</Title>
        <ul>
          {json_translate.part3.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
               
                <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                    {t(`part3.content.${i}.text`)}
                  </Text> 
              </li>
              
            </Row>
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part4.title')}</Title>
        <ul>
          {json_translate.part4.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part4.content.${i}.text`)}
                  </Text> 
                  <ul>
                    {_.contentt?.map((_,index)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part4.content.${i}.contentt.${index}.text`)}
                    </Text> </li>
                    ))}
                  </ul>                 
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part5.title')}</Title>
        <ul>
          {json_translate.part5.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
            <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part5.content.${i}.text`)}
                  </Text> 
                  <ul>
                    {_.contentt?.map((__,index)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part5.content.${i}.contentt.${index}.text`)}
                    </Text>
                    <ul>
                    {__.contenttt?.map((___,indexx)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part5.content.${i}.contentt.${index}.contenttt.${indexx}.text`)}
                    </Text> 
                    
                    </li>
                    ))}
                  </ul> 

                    </li>
                    ))}
                  </ul>                 
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
      <Title {...title_layout}>{t('part6.title')}</Title>
        <ul>
          {json_translate.part6.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
               
                <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                    {t(`part6.content.${i}.text`)}
                  </Text>
                
              </li>
              
            </Row>
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part7.title')}</Title>
        <ul>
          {json_translate.part7.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part7.content.${i}.text`)}
                  </Text> 
                  <ul>
                    {_.contentt?.map((__,index)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part7.content.${i}.contentt.${index}.text`)}
                    </Text>
                    <ul>
                    {__.contenttt?.map((___,indexx)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part7.content.${i}.contentt.${index}.contenttt.${indexx}.text`)}
                    </Text> 
                    
                    </li>
                    ))}
                  </ul> 

                    </li>
                    ))}
                  </ul>                 
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part8.title')}</Title>
        <ul>
          {json_translate.part8.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part8.content.${i}.text`)}
                  </Text> 
                  <ul>
                    {_.contentt?.map((_,index)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part8.content.${i}.contentt.${index}.text`)}
                    </Text> </li>
                    ))}
                  </ul>                 
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part9.title')}</Title>
        <ul>
          {json_translate.part9.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part9.content.${i}.text`)}
                  </Text> 
                  <ul>
                    {_.contentt?.map((_,index)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part9.content.${i}.contentt.${index}.text`)}
                    </Text> </li>
                    ))}
                  </ul>                 
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part10.title')}</Title>
        <ul>
          {json_translate.part10.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part10.content.${i}.text`)}
                  </Text> 
                                  
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part11.title')}</Title>
        <ul>
          {json_translate.part11.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part11.content.${i}.text`)}
                  </Text> 
                  <ul>
                    {_.contentt?.map((_,index)=>(
                      <li><Text type='secondary' style={{ fontSize: '1.2em' }}>
                      {t(`part11.content.${i}.contentt.${index}.text`)}
                    </Text> </li>
                    ))}
                  </ul>                 
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
      <Col {...responsive_constant}>
        <Title {...title_layout}>{t('part12.title')}</Title>
        <ul>
          {json_translate.part12.content.map((_, i) => (
            <Row style={{ margin: '20px  0' }}>
              <li>
                  <Text type='secondary' style={{ fontSize: '1.2em' }}>
                    {/*con.text*/} 
                     {t(`part12.content.${i}.text`)}
                  </Text>             
              </li>   
            </Row>
              
                
          ))}
        </ul>
      </Col>
    </Row>
  );
};
export default index;
