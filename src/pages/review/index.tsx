import React, { FC } from "react";
import { Col, Form, Row, Typography } from "antd";
import useTranslation from "next-translate/useTranslation";
import { responsive_constant } from "../../constants/layout/responsive";
import { Product } from "../../models";
import AddReview from "../../components/review";
import { BText } from "../../constants/shared/b-text";
import ProductCard from "../../components/product-card";
import Slider from "../../components/slider";

interface props {
  product: Product;
}

const Review: FC<props> = ({ product }) => {
  const { t } = useTranslation("review");

  return (
    <Row justify="center" gutter={[0, 40]} style={{ marginTop: 20 }}>
      <Col {...responsive_constant}>
        <BText lvl={1} t={1} fw="bolder" style={{ textTransform: "uppercase" }}>
          {t("add-review")}
        </BText>
        <AddReview product={product} />
      </Col>
    </Row>
  );
};

export default Review;
