export interface Review_Req {
    rating: number;
    review_text: string;
    product_id:number;
  }


export interface Review{
    rating: number;
    review_text: string;
  }
  

  export interface Review_I_Req {
    review: Review_Req;
  }
  