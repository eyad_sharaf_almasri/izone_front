import { Product } from '../product';

export interface Search_Result_Model {
  id: number;
  brand_id: number;
  category_id: number;
  name: string;
  overview: string;
  specifications: string;
  product_images: { id: number; image_path: string }[];
  product_tags: { id: number; name: string }[];
  price: number;
  status: boolean;
  message: string;
  code: number;
  paginate: any;
  similar_products: Product[];
  default_price: number;
  price_reseller: number;
  price_distributor: number;
  price_after_discount:number;
  rating:number;
  discount:number;
  key_components:string;
  how_to_use:string;
  brand:{
    id: number;
    logo: string;
    name: string;
  }
  reting:number;
  
  reviews: {
    id:number;
    review_text:string;
    rating:number;
  }[]

  top_reviews: {
    id:number;
    review_text:string;
    rating:number;
  }[]
}

export interface Search_Request_Model {
  tags?: any[];
  brand?: string;
}
