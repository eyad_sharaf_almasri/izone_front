import { Layout } from "antd";
import React, { useState } from "react";
import Image from "next/image";
import MAWNSider from "./sider";
import Head from "next/head";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";

const { Content, Sider } = Layout;

const DahsboardLayout: React.FC = ({ children }) => {
  const { lang } = useTranslation();

  const [marginContent, setMarginContent] = useState(80);

  const collapsed = marginContent === 80;

  return (
    <Layout className="site-layout" style={{ minHeight: "100vh" }} hasSider>
      <Head>
        <title>IZONE Dashboard</title>
      </Head>
      <Sider
        reverseArrow={lang === "ar"}
        style={{
          height: "100%",
          overflow: "auto",
          position: "fixed",
          left: lang === "en" ? 0 : undefined,
          right: lang === "en" ? undefined : 0,
        }}
        onCollapse={(isCol: boolean) =>
          setMarginContent(() => (isCol ? 80 : 200))
        }
        theme="dark"
        defaultCollapsed
        collapsible
      >
        <div style={{ padding: 5 }}>
          <Image
            src="/assets/izone.png"
            layout="responsive"
            width={350}
            height={159}
          />
        </div>
        <MAWNSider />
      </Sider>

      <Content
        style={
          lang === "en"
            ? { marginLeft: marginContent }
            : { marginRight: marginContent }
        }
      >
        <div style={{ margin: "0 16px" }}>{children}</div>
      </Content>
    </Layout>
  );
};
export default DahsboardLayout;
