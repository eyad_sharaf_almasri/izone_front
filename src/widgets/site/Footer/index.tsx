import React from "react";
import { Col, Layout, Row } from "antd";
import { primaryColor, secondaryColor } from "../../../constants/layout/color";

import "./style.less";
import BottomFooter from "./bottom-footer";
import FooterLinks from "./footer-link";

export default function Footer() {
  return (
    // <Row style={{ fontWeight: "normal", backgroundColor: secondaryColor }} justify="center" gutter={[0, 4]}>
    //   <Col lg={6} sm={24} xs={24}>
    //     <BottomFooter />
    //   </Col>
    //   <Col lg={18} sm={24} xs={24} style={{ padding: 10 }}>
    //     <FooterLinks />
    //   </Col>
    // </Row>
<>
      <div style={{ backgroundColor: primaryColor, height: 10 }} />
      <FooterLinks />
      <BottomFooter />
      </>
  );
}
