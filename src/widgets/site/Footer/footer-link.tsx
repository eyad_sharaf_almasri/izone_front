import {
  FacebookFilled,
  InstagramFilled,
  LinkedinFilled,
  PhoneFilled,
} from "@ant-design/icons";
import { Button, Col, Form, Input, notification, Row, Space } from "antd";
import useTranslation from "next-translate/useTranslation";
import Link from "next/link";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  secondaryColor,
  responsive_constant,
  primaryColor,
} from "../../../constants/layout";
import { BButton } from "../../../constants/shared/b-button";
import { BText } from "../../../constants/shared/b-text";
import { TelegramFilled } from "../../../constants/svgs/telegram-filled";
import { selectEmailsStatus, InsertEmailAsync } from "../../../redux/email";

interface Props {}

const FooterLinks: React.FC<Props> = () => {
  const { t } = useTranslation("footer");
  const dispatch = useDispatch();
  const status = useSelector(selectEmailsStatus);

  const handleConfirm = (req: { email: string }) => {
    dispatch(InsertEmailAsync(req));
  };

  useEffect(() => {
    status === "data" &&
      notification.success({
        message: t`bottom-footer.Thanks for submitting your email`,
        duration: 2,
        placement: "bottomRight",
      });
  }, [status]);

  return (
    <Row
      justify="center"
      style={{ padding: "40px 0", backgroundColor: secondaryColor }}
    >
      <Col {...responsive_constant}>
        <Row gutter={[0, 32]}>
          <Col span={24}>
            <Row justify="space-between" gutter={{ lg: 32, xs: 0 }}>
              <Col lg={8} span={12}>
                <Space direction="vertical" size="small">
                  <Link href="/">
                    <a>
                      <img src="/assets/izone.png" width={250} alt="LOGO" />
                    </a>
                  </Link>
                  <Link href="/about">
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}}>
                        {t`bottom-footer.about-us`}
                      </BButton>
                    </a>
                  </Link>
                  <Link href="/terms-conditions">
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}}>
                        {t`bottom-footer.terms-of-use`}
                      </BButton>
                    </a>
                  </Link>
                
                </Space>
              </Col>

              <Col lg={8} span={12}>
                <Space direction="vertical" size="small">
                  <BText color="primary" fw="bold" lvl={3}>
                    {t`bottom-footer.cutomer-services`}
                  </BText>
                  <Link href="/contact">
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}}>
                        {t`bottom-footer.contact-us`}
                      </BButton>
                    </a>
                  </Link>
                  <Link href="/privacy-policy">
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}} >
                        {t`bottom-footer.privacy-policy`}
                      </BButton>
                    </a>
                  </Link>
                </Space>
              </Col>
              <Col lg={8} span={12}>
                <Space direction="vertical" size="small">
                  <BText color="primary" fw="bold" lvl={3}>
                    {t`bottom-footer.my-account`}
                  </BText>
                  <Link href="/personal-collection" >
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}}>
                        {t`bottom-footer.my-profile`}
                      </BButton>
                    </a>
                  </Link>
                  <Link href="/cart">
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}}>
                        {t`bottom-footer.my-cart`}
                      </BButton>
                    </a>
                  </Link>
                  <Link href="/wishlist">
                    <a>
                      <BButton type="text" color="white" style={{fontSize:'1rem'}}>
                        {t`bottom-footer.my-wish-list`}
                      </BButton>
                    </a>
                  </Link>
                </Space>
              </Col>
            </Row>
          </Col>

          <Col span={24}>
            <Row justify="space-between" gutter={{ lg: 32, xs: 0 }}>
              <Col lg={8} span={12}>
                <Row gutter={[0, 16]}>
                  <Col span={24}>
                    <BText color="primary" fw="bold" lvl={3}>
                      {t`bottom-footer.download-the-app`}
                    </BText>
                  </Col>
                  <Col span={12}>
                    <Row gutter={[16, 16]}>
                      <Space direction="horizontal">
                        <a>
                          <img
                            src="/assets/google-play.png"
                            width={150}
                            alt="GOOGLE_PLAY"
                          />
                        </a>{" "}
                        <a>
                          <img
                            src="/assets/app-store.png"
                            width={150}
                            alt="APP_STORE"
                          />
                        </a>
                      </Space>
                    </Row>
                  </Col>
                </Row>
              </Col>

              <Col lg={8} span={12}>
                <Row gutter={[0, 16]}>
                  <Col span={24}>
                    <BText color="primary" fw="bold" lvl={3}>
                      {t`bottom-footer.subscribe-to-newsletter`}
                    </BText>
                  </Col>
                  <Col span={24}>
                    <Form onFinish={handleConfirm}>
                      <Row wrap={false} dir="ltr" style={{ direction: "ltr" }}>
                        <Col flex="auto">
                          <Form.Item
                            labelCol={{ span: 24 }}
                            name="email"
                            rules={[
                              {
                                required: true,
                                message: t`bottom-footer.required`,
                              },
                              {
                                type: "email",
                                message: t`bottom-footer.type_email`,
                              },
                            ]}
                          >
                            <Input
                              size="large"
                              placeholder={t`bottom-footer.enter-email-address`}
                              style={{
                                borderRadius: "5px 0 0 5px",
                                width: "100%",
                                
                              }}
                            />
                          </Form.Item>
                        </Col>

                        <Col flex="150px">
                          <Button
                            size="large"
                            type="primary"
                            style={{ borderRadius: "0 5px 5px 0" }}
                            block
                            loading={status === "loading"}
                            htmlType="submit"
                          >
                            {t`bottom-footer.subscribe`}
                          </Button>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                </Row>
              </Col>

              <Col lg={8} span={24} style={{ paddingTop: 10 }}>
                <Row justify="space-around">
                  <Col>
                    <Space direction="vertical" align="center" size="small">
                      <PhoneFilled
                        style={{ color: "white", fontSize: "1rem" }}
                      />
                      <TelegramFilled
                        style={{ color: "white", fontSize: "1rem" }}
                      />
                      <BText color="white">{t`bottom-footer.follow-us`}</BText>
                    </Space>
                  </Col>
                  <Col>
                    <Space
                      direction="vertical"
                      align="center"
                      size="small"
                      style={{ direction: "ltr" }}
                    >
                      <BText color="white" style={{ direction: "ltr", fontSize: "1.2rem" }}>
                        {"+971 55 838 9550"}
                      </BText>
                      <BText style={{ fontSize: "1.2rem" }} color="white">{"sales@izone-me.com"}</BText>
                      <Space direction="horizontal" size="large">
                        <FacebookFilled
                          style={{ color: "white", fontSize: "1.2rem" }}
                        />
                        <InstagramFilled
                          style={{ color: "white", fontSize: "1.2rem" }}
                        />
                        <LinkedinFilled
                          style={{ color: "white", fontSize: "1.2rem" }}
                        />
                        <TelegramFilled
                          style={{ color: "white", fontSize: "1.2rem" }}
                        />
                      </Space>
                    </Space>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default FooterLinks;
