import { Col, Row } from "antd";
import React, { FC } from "react";
import { primaryColor, responsive_constant } from "../../../constants/layout";
import { BText } from "../../../constants/shared/b-text";

const BottomFooter: FC = () => {
  return (
    <Row justify="center" style={{ backgroundColor: primaryColor, padding: "5px 0" }}>
      <Col {...responsive_constant}>
        <Row justify="center" align="middle">
          <Col>
            <BText lvl={3} color="white">
              {"IZONE Solutions  2020-2021©"}
            </BText>
          </Col>
        </Row>
      </Col>
    </Row>
  );
};
export default BottomFooter;
