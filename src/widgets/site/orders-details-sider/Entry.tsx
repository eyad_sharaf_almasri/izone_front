import { DeleteOutlined } from "@ant-design/icons";
import { Row, Col, Typography, Button } from "antd";
import Title from "antd/lib/skeleton/Title";
import useTranslation from "next-translate/useTranslation";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Product } from "../../../models";
import id from "../../../pages/brand/[id]";
import { DeleteItem } from "../../../redux";
import thousands_separators from "../../../utils/helpers/thousands_separators";
const { Text } = Typography;

interface propsInterface {
  src: string;
  details: string;
  price: number;
  quantity: number;
  id: number;
  deleteItem: (id: number) => void;
}

const Entry: React.FC<propsInterface> = ({
  src,
  details,
  price,
  quantity,
  id,
  deleteItem,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation("cart");

  return (
    <Row
      justify="space-between"
      style={{
        padding: "10px",
        width: "94%",
        marginBottom: 20,
        borderBottom: "1px solid #e6e6e6",
      }}
    >
      <Col
        span={16}
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "space-between",
        }}
      >
        <Text>{details}</Text>
        <Text style={{ color: "#2B2E82", marginBottom: 0 }}>
          {t(`quantity`, { num: quantity })}
        </Text>
        <Button
          style={{
            flex: "0 0 1%",
            padding: 1,
            margin: 0,
            border: "none",
            backgroundColor: "transparent",
          }}
          onClick={() => deleteItem(id)}
        >
          <DeleteOutlined />
        </Button>
      </Col>

      <Col span={8} style={{ textAlign: "center", padding: "0 5px" }}>
        <img
          src={src}
          style={{
            maxWidth: "100%",
            border: "2px solid #aba8a8",
            textAlign: "center",
            display: "block",
            borderRadius: 5,
          }}
        />
        <Text
          style={{ margin: "5px 0 0 0", color: "#2B2E82", fontWeight: "bold" }}
        >
          {thousands_separators(price)} SYR
        </Text>
      </Col>
    </Row>
  );
};

export default Entry;
