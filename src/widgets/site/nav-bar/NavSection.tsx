import useTranslation from "next-translate/useTranslation";
import Link from "next/link";
import { useRouter } from "next/router";
import { HomeFilled , AppstoreFilled } from "@ant-design/icons";

const linkStyle: React.CSSProperties = {
  color: "#fff",
  display: "inline-block",
  textAlign: "left",
  paddingLeft: 5,
  textTransform: "capitalize",
  textUnderlineOffset: 10,
};

const NavSection: React.FC = () => {
  const { t } = useTranslation("navbar");
  const { pathname } = useRouter();

  return (
    <nav
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        width: "60%",
        fontSize: "1em",
      }}
    >
      <Link href="/">
        <a style={{ ...linkStyle, textDecoration: pathname === "/" ? "underline" : "none" }}><img src="assets/navIcons/home.svg"  style={{paddingInline:10}} />{t`links.home`}</a>
      </Link>

      {/* <Link href='/about'>
        <a style={{ ...linkStyle, textDecoration: pathname === '/about' ? 'underline' : 'none' }}>{t`links.about-us`}</a>
      </Link> */}

      <Link href="/brands">
        <a style={{ ...linkStyle, textDecoration: pathname === "/brands" ? "underline" : "none" }}><img src="assets/navIcons/brand.svg" style={{paddingInline:10}} />{t`links.brands`}</a>
      </Link>

      {/* <Link href='/contact'>
        <a style={{ ...linkStyle, textDecoration: pathname === '/contact' ? 'underline' : 'none' }}>{t`links.contact-us`}</a>
      </Link> */}

      {/* <Link href='/jobs'>
        <a style={{ ...linkStyle, textDecoration: pathname === '/jobs' ? 'underline' : 'none' }}>{t`links.jobs`}</a>
      </Link> */}

      <Link href="/check-order">
        <a style={{ ...linkStyle, textDecoration: pathname === "/check-order" ? "underline" : "none" }}><img src="assets/navIcons/support.svg" style={{paddingInline:10}} />{t`links.check-order`}</a>
      </Link>
    </nav>
  );
};

export default NavSection;
