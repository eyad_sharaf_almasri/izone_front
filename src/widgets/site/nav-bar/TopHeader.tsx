import { Col, Dropdown, Menu, Row, Space, Button, Typography } from "antd";
import React, { FC, useState } from "react";
import useTranslation from "next-translate/useTranslation";
import NextLink from "next/link";
import { useRouter } from "next/router";
import { LanguageOutlined } from "../../../constants/svgs";
import Icon, { LoadingOutlined, PhoneOutlined } from "@ant-design/icons";

import { Lang, Phone } from "../../../constants/svgs";
import { appServices } from "../../../services";
const iconStyle: React.CSSProperties = {
  color: "#fff",
  fontSize: "1em",
  display: "flex",
  alignItems: "center",
};
const { Title, Link, Text } = Typography;

const TopHeader: FC = () => {
  const { replace, asPath } = useRouter();
  const [loading, setLoading] = useState(false);
  const { t, lang } = useTranslation("navbar");
  const en = lang === "en";

  const handleLang = async (key: string) => {
    setLoading(true);
    await appServices.changeLang({ lang: key as any });
    replace(asPath, undefined, { locale: key });
    setLoading(false);
  };

  const menuLang = (
    <Menu onClick={(e) => handleLang(e.key.toString())}>
      <Menu.Item key="en">{t`nav-en`}</Menu.Item>
      <Menu.Item key="ar">{t`nav-ar`}</Menu.Item>
    </Menu>
  );

  return (
    <>
      <Dropdown
        overlay={menuLang}
        arrow={true}
        placement="bottomCenter"
        trigger={["hover"]}
      >
        <Button type="text" style={{ left: en ? -3 : 3 }}>
          <Space direction="vertical" size="small">
            {loading ? (
              <LoadingOutlined
                spin={loading}
                style={{ ...iconStyle, marginTop: 5 }}
              />
            ) : (
              <Button
                type="link"
                style={{ color: "#fff", display: "flex", fontSize: "1em" }}
                icon={
                  <LanguageOutlined
                    style={{ ...iconStyle, width: "30px", height: "30px" }}
                  />
                }
              >
                {lang === "en" ? (
                  <Text style={{ color: "#fff" }}>{t`nav-en`}</Text>
                ) : (
                  <Text style={{ color: "#fff" }}>{t`nav-ar`}</Text>
                )}
              </Button>
            )}
          </Space>
        </Button>
      </Dropdown>

      <NextLink href={"/contact"}>
        <Button
          type="link"
          style={{ color: "#fff", fontSize: "1em" }}
          icon={
            lang === "en" ? (
              <PhoneOutlined
                style={{ fontSize: "1em", transform: "rotate(90deg)" }}
              />
            ) : (
              <PhoneOutlined style={{ fontSize: "1em" }} />
            )
          }
        >{t`contact-us`}</Button>
      </NextLink>
    </>
    // icon={<IoLanguage style={{marginRight:10}} />}
    // icon={<IoCall style={{marginRight:10}} />}
  );
};

export default TopHeader;
