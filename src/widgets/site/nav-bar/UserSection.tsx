import {
  Badge,
  Button,
  Space,
  Menu,
  Dropdown,
  Typography,
  Modal,
  Col,
  Row,
  notification,
} from "antd";
import Icon, {
  UserOutlined,
  LoadingOutlined,
  AppstoreFilled,
  DownOutlined,
  HeartFilled,
  HomeFilled,
  SearchOutlined,
  ShoppingCartOutlined,
} from "@ant-design/icons";

import Link from "next/link";

import { useDispatch, useSelector } from "react-redux";
import { logoutAsync, selectUser } from "../../../redux/app";
import { selectCart, selectQuantityItem } from "../../../redux";
import { LanguageOutlined } from "../../../constants/svgs";
import useTranslation from "next-translate/useTranslation";
import { useRouter } from "next/router";
import { appServices } from "../../../services";
import React, { useState } from "react";
import { cartSvg } from "../Footer/social-icons";

const iconStyle: React.CSSProperties = {
  color: "#fff",
  fontSize: "1.2em",
  display: "flex",
  alignItems: "center",
};
const { Title, Text } = Typography;
const { confirm } = Modal;

const UserSection: React.FC = () => {
  const [loading, setLoading] = useState(false);
  const { t, lang } = useTranslation("navbar");
  const en = lang === "en";
  const dispatch = useDispatch();
  const { push, replace, asPath, pathname } = useRouter();

  //-------------------------------------

  const onClick = async (key: string) => {
    switch (key) {
      case "logout":
        confirm({
          title: t`confirm-logout`,
          onOk: () => {
            dispatch(logoutAsync());
            replace("/");
          },
          onCancel: () => {},
          centered: true,
        });
        break;

      case "info":
        push("personal-collection");
        break;

      default:
        return null;
    }
  };
  const cartNotification = (msg: string, description: string, type: string) => {
    notification[type === "success" ? "success" : "warning"]({
      message: msg,
      description: description,
      placement: "bottomRight",
      duration: 3,
    });
  };
  const menu = (
    <Menu onClick={(e) => onClick(e.key.toString())}>
      <Menu.Item key="info">{t`p-info`}</Menu.Item>
      <Menu.Item key="logout">{t`sign-out`}</Menu.Item>
    </Menu>
  );

  const user = useSelector(selectUser);

  const cart = useSelector(selectCart);

  const totalNumberOfProductswishlis = useSelector(selectQuantityItem);

  const totalNumberOfProducts = cart.reduce(
    (acc, current) => acc + current.quantity,
    0
  );
  // const totalNumberOfProductswishlis = wishListItem.reduce((acc, current) => acc + current.quantity, 0);
  const { Text } = Typography;

  return (
    <Row
      justify="center"
      align="middle"
      style={{
        width: "100%",
        padding: "5px 0",
      }}
    >
      <Col span={8} style={{ display: "flex", justifyContent: "center" }}>
        {!pathname.match(/\/cart/) && (
          <Badge
            count={totalNumberOfProducts}
            style={{
              border: "none",
              boxShadow: "none",
              top: "-6px",
              left: `${lang === "ar" ? "25px" : "15px"}`,
              padding: "0.5px 1px 0 0",
            }}
          >
            <Link href="/cart">
              <Button
                type="text"
                size="large"
                icon={
                  <img src="/assets/navIcons/cart.svg" style={{ fontSize: 30, color: "#fff",height:'80%',objectFit:'contain'  }} />
                }
              ></Button>
            </Link>
          </Badge>
        )}
      </Col>
      <Col span={8} style={{ display: "flex", justifyContent: "center" }}>
        {user && !pathname.match(/\/wishlist/) ? (
          <Badge
            count={totalNumberOfProductswishlis}
            style={{
              border: "none",
              boxShadow: "none",
              top: "-6px",
              left: `${lang === "ar" ? "25px" : "15px"}`,
              padding: "0.5px 1px 0 0",
            }}
          >
            <Link href="/wishlist">
              <Button
                type="text"
                size="large"
                icon={<img src="/assets/navIcons/heart.svg" style={{ fontSize: 30, color: "#fff",height:'100%',width:'100%',objectFit:'contain'  }} />}
              ></Button>
            </Link>
          </Badge>
        ) : (
          <Link href="/wishlist">
            <Button
              type="text"
              size="large"
              icon={<img src="/assets/navIcons/heart.svg" style={{ fontSize: 30, color: "#fff",height:'100%',width:'100%',objectFit:'contain'  }} />}
            ></Button>
          </Link>
        )}
      </Col>

      <Col span={8} style={{ display: "flex", justifyContent: "center" }}>
        {user ? (
          <Dropdown
            overlay={menu}
            arrow={true}
            placement="bottomCenter"
            trigger={["hover"]}
          >
            <Button
              type="text"
              style={{ display: "flex", alignItems: "center" }}
            >
              <Space direction="horizontal" size="small">
                <Text style={{ fontSize: 30, color: "#fff" }}>
                  {user.first_name}
                </Text>
              </Space>
            </Button>
          </Dropdown>
        ) : (
          <Link href="/login">
            <Button
              type="text"
              size="large"
              icon={<img src="/assets/navIcons/profile.svg" style={{ fontSize: 30, color: "#fff",height:'100%',width:'100%',objectFit:'contain' }} />}
              className="badge"
            />
          </Link>
        )}
      </Col>
    </Row>
  );
};

export default UserSection;
