import React from 'react';
import { Input, Button, Col, Spin } from 'antd';
import styles from './navbar.module.css';
import { useSelector } from 'react-redux';
import CustomeSelect from './CustomeSelect';
import { LoadingOutlined, SearchOutlined } from '@ant-design/icons';
import useTranslation from 'next-translate/useTranslation';
import { selectRequestedProductsStatus } from '../../../redux';

const antIcon = <LoadingOutlined style={{ fontSize: 20 }} spin />;

interface propsInterface {
  searchCategoryHandler: (val: number) => void;
  handleSearchChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleSearchSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
  searchString: string;
  styleObj?: {};
}

const Searchbar: React.FC<propsInterface> = ({
  searchCategoryHandler,
  handleSearchChange,
  handleSearchSubmit,
  searchString,
  styleObj,
}) => {
  const { t } = useTranslation('navbar');

  const requested_products_status = useSelector(selectRequestedProductsStatus);

  return (
    <div style={{direction:'rtl'}}>
      <form
        style={{
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
          outline: 'none',
          ...styleObj,
        }}
        onSubmit={handleSearchSubmit}
      >
        <Button
          htmlType='submit'
          className={`${styles.searchBarBtn} ${styles.searchBtn}`}
        >
          {requested_products_status === 'loading' ? (
            <Spin indicator={antIcon} />
          ) : (
            <SearchOutlined />
          )}
        </Button>
        <Input
          dir='rtl'
          placeholder={t`search-product`}
          style={{
            border: 'none',
            padding: '10px 15px 10px 10px',
            width: "80%",
            borderTopLeftRadius: 10,
            borderBottomLeftRadius:10,
            borderTopRightRadius:0,
            borderBottomRightRadius:0,
            height: 35,
            outline: 'none',
            fontSize:14
          }}
          value={searchString}
          onChange={handleSearchChange}
        />
        {/* <CustomeSelect searchCategoryHandler={searchCategoryHandler} /> */}
      </form>
    </div>
  );
};

export default Searchbar;
