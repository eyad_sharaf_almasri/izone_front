// React and React Components
import React, { useEffect, useState, useMemo } from "react";
import NavSection from "./NavSection";
import UserSection from "./UserSection";
import Toptop from "./TopHeader";
import Searchbar from "./Searchbar";
// Next
import { useRouter } from "next/router";
import Link from "next/link";
// import Image from "next/image";
// Ant
import {
  Row,
  Col,
  Select,
  Drawer,
  Button,
  Modal,
  Space,
  Dropdown,
  Menu,
  Badge,
  Typography,
  Image,
} from "antd";
const { confirm } = Modal;
import Icon, {
  MenuOutlined,
  SearchOutlined,
  ShoppingCartOutlined,
  RightOutlined,
  LeftOutlined,
  DownOutlined,
} from "@ant-design/icons";
// Style sheets
import styles from "./navbar.module.css";
// other imports
import useTranslation from "next-translate/useTranslation";
import { responsive_constant } from "../../../constants/layout/responsive";
import { appServices } from "../../../services";
import { primaryColor } from "../../../constants/layout/color";
import { useDispatch, useSelector } from "react-redux";
import {
  FetchCategoriesAsync,
  FetchSiteCategoriesAsync,
  logoutAsync,
  selectCategories,
  selectBrands,
  selectCart,
  selectRequestedProductsStatus,
  selectUser,
} from "../../../redux";
import { emailSvg, whatsappSvg } from "../Footer/social-icons";
import { linerBackground } from "../../../constants/layout";
import "./style.less";
// interface
interface propsInterface {
  imgSrc?: string;
  searchCategoryHandler: (val: number) => void;
  handleSearchChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  handleSearchSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
  searchString: string;
}

// ======================================
// Large screens navbar
const { SubMenu } = Menu;
const { Text } = Typography;
export const LargeNav: React.FC<propsInterface> = ({
  searchCategoryHandler,
  handleSearchChange,
  handleSearchSubmit,
  searchString,
}) => {
  const { t, lang } = useTranslation("navbar");
  const { push } = useRouter();
  const categories = useSelector(selectCategories);
  const brands = useSelector(selectBrands);
  const dispatch = useDispatch();

  const handlePageNavigation = async (val: string) => {
    push(`/brand/${val}`);
  };

  useEffect(() => {
    dispatch(FetchSiteCategoriesAsync(0));
  }, [lang]);

  const categories_menu = (
    <Menu
      mode="vertical"
      style={{ width: "100%" }}
      triggerSubMenuAction="click"
      expandIcon={
        lang === "en" ? (
          <RightOutlined style={{ fontSize: "0.8em", top: 15, right: 20 }} />
        ) : (
          <LeftOutlined
            style={{ fontSize: "0.8em", top: 15, left: "-80%", right: 0 }}
          />
        )
      }
    >
      {categories.map((category) => (
        <SubMenu
          key={category.id}
          title={<span style={{ padding: "0 15px" }}>{category.name}</span>}
          icon={<img src={category.image_path} style={{ width: 40 }} />}
          style={{
            width: "100%",
            borderBottom: "1px solid gray",
            padding: 10,
            height: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "start",
          }}
          // onTitleClick={() => dispatch(FetchSiteCategoriesAsync(category.id))}
        >
          <Col
            style={{
              width: "800px",
              height: "400px",
              overflow: "auto",
              listStyle: "none",
            }}
          >
            <Row>
              <Col
                lg={18}
                sm={24}
                style={{
                  borderRight: "1px solid gray",
                  borderBottom: "none",
                }}
              >
                <Row>
                  <Col>
                    <Row>
                      {category?.sub_categories?.map((subcategory) => (
                        <Link href={`/sub-category/${subcategory.id}`}>
                          <div style={{ padding: 20 }} key={subcategory.id}>
                            <Col xs={24} lg={6} sm={12}>
                              <Image
                                style={{ cursor: "pointer" }}
                                preview={false}
                                width={150}
                                src={subcategory.image_path}
                              />
                              <Text
                                style={{
                                  display: "flex",
                                  padding: "0px 0px 20px 0px",
                                  justifyContent: "center",
                                  width: "100%",
                                }}
                              >
                                {subcategory.name}
                              </Text>
                            </Col>
                          </div>
                        </Link>
                      ))}
                    </Row>
                  </Col>
                </Row>
              </Col>
              <Col lg={6} sm={24}>
                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap",
                    justifyContent: "space-around",
                    margin: "20px 0",
                  }}
                >
                  {brands.map((brand) => (
                    <Link href={`/brand/${brand.id}`}>
                      <div key={brand.id}>
                        <Image
                          preview={false}
                          style={{ margin: "0 0 15px 0", cursor: "pointer" }}
                          width={130}
                          src={brand.logo}
                        />
                      </div>
                    </Link>
                  ))}
                </div>
              </Col>
            </Row>
          </Col>
        </SubMenu>
      ))}
    </Menu>
  );

  return (
    <>
      <Row
        justify="center"
        align="middle"
        style={{
          background: primaryColor,
          padding: "5px 0",
        }}
      >
        {/* Top Section */}

        <Col span={23}>
          <Row justify="space-between" align="middle">
            <Col
              style={{
                flex: "1 1 150px",
                display: "flex",
                justifyContent: "flex-start",
                alignItems: "center",
                color: "#fff",
                fontSize: "1rem",
                height: 40,
              }}
            >
              <span>
                <a
                  style={{
                    color: "#fff",
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    minWidth: "145px",
                    direction: "ltr",
                  }}
                  target="_blank"
                  href="https://wa.me/009668003040500"
                >
                  <Icon
                    style={{ marginBottom: 2, marginRight: 2 }}
                    component={whatsappSvg}
                  />
                  +9668003040500
                </a>
              </span>
            </Col>
            <Col
              style={{
                height: 40,
                flex: "1 1 50%",
                display: "flex",
                alignItems: "center",
                justifyContent: "flex-end",
              }}
            >
              <Toptop />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row
        justify="center"
        align="middle"
        style={{
          background: "#333",
          padding: "5px 0",
        }}
      >
        {/* Top Section */}

        <Col {...responsive_constant}>
          <Row justify="space-between" align="middle">
            <Col flex="1 1 0px">
              <Link href="/">
                <img
                  src="/assets/izone.png"
                  style={{
                    maxWidth: "200px",
                    cursor: "pointer",
                    marginTop: 10,
                  }}
                />
              </Link>
            </Col>

            <Col
              style={{
                height: 40,
                flex: "1 1 50%",
                display: "flex",
                alignItems: "center",
              }}
            >
              <NavSection />
            </Col>
          </Row>
        </Col>
      </Row>

      {/* Bottom Section */}

      <Row
        justify="center"
        align="middle"
        style={{
          background: "#333",
          padding: "0 5px 20px",
        }}
      >
        {/* Top Section */}

        <Col {...responsive_constant}>
          <Row
            justify="space-between"
            align="middle"
            style={{ fontSize: "0.9rem", position: "relative", zIndex: 999 }}
            gutter={[10,0]}
          >
            <Col
              span={5}
              style={{
                justifyContent: "end",
                height: 50,
                display: "flex",
                paddingTop: 3,
              }}
            >
              <Dropdown
                overlay={categories_menu}
                placement="bottomCenter"
                trigger={["hover"]}
              >
                <Space
                  className="navdropdown"
                  direction="horizontal"
                  size="middle"
                >
                  <MenuOutlined
                    style={{ color: "#fff", fontSize: "1.6em", marginTop: 24 }}
                  />
                  <Button
                    style={{
                      backgroundColor: "transparent",
                      color: "#fff",
                      border: "none",
                      padding: 0,
                      height: 40,
                      display: "flex",
                      alignItems: "center",
                      width: 171,
                    }}
                  >
                    <Text
                      style={{ fontSize: "1em", color: "#fff", width: "100%" }}
                    >{t`categories`}</Text>
                    <DownOutlined
                      style={{ fontSize: "0.7em", marginLeft: "auto" }}
                    />
                  </Button>
                </Space>
              </Dropdown>
            </Col>
            <Col span={13}>
              <Searchbar
                searchCategoryHandler={searchCategoryHandler}
                handleSearchChange={handleSearchChange}
                handleSearchSubmit={handleSearchSubmit}
                searchString={searchString}
              />
            </Col>
            <Col
              span={6}
              style={{
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <UserSection />
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

// ======================================
// small screens navbar

export const SmallNav: React.FC<propsInterface> = ({
  searchCategoryHandler,
  handleSearchChange,
  handleSearchSubmit,
  searchString,
}) => {
  const { lang } = useTranslation();
  const user = useSelector(selectUser);
  const { replace, route, push } = useRouter();
  const { Option } = Select;
  const [loading, setLoading] = useState(false);
  const { t } = useTranslation("navbar");
  const requested_products_status = useSelector(selectRequestedProductsStatus);
  const categories = useSelector(selectCategories);
  const brands = useSelector(selectBrands);
  const dispatch = useDispatch();
  // Drawer controllers
  const [visible, setVisible] = useState(false);

  const { pathname } = useRouter();

  const cart = useSelector(selectCart);
  const totalNumberOfProducts = cart.reduce(
    (acc, current) => acc + current.quantity,
    0
  );

  const openDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  // Modal controllers
  const [isModalVisible, setIsModalVisible] = useState(false);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    if (requested_products_status === "data" && isModalVisible) {
      setIsModalVisible(false);
    }
  }, [requested_products_status]);

  useEffect(() => {
    dispatch(FetchSiteCategoriesAsync(0));
  }, [lang]);

  useEffect(() => {
    onClose();
  }, [route]);

  const handlePageNavigation = async (val: string) => {
    push(`/brand/${val}`);
    setVisible(false);
  };

  const categories_menu = (
    <Menu
      mode="vertical"
      style={{ width: "100%" }}
      triggerSubMenuAction="click"
      expandIcon={
        lang === "en" ? (
          <RightOutlined style={{ fontSize: "0.8em", top: 15, right: 20 }} />
        ) : (
          <LeftOutlined
            style={{ fontSize: "0.8em", top: 15, left: "-80%", right: 0 }}
          />
        )
      }
    >
      {categories.map((category) => (
        <SubMenu
          key={category.id}
          title={<span style={{ padding: "0 15px" }}>{category.name}</span>}
          icon={<img src={category.image_path} style={{ width: 40 }} />}
          style={{
            width: "100%",
            borderBottom: "1px solid gray",
            padding: 10,
            height: "50px",
            display: "flex",
            alignItems: "center",
            justifyContent: "start",
          }}
          // onTitleClick={() => dispatch(FetchSiteCategoriesAsync(category.id))}
        >
          <Col
            style={{
              width: "800px",
              height: "400px",
              overflow: "auto",
              listStyle: "none",
            }}
          >
            <Row>
              <Col
                lg={18}
                sm={24}
                style={{
                  borderRight: "1px solid gray",
                  borderBottom: "none",
                }}
              >
                <Row>
                  <Col>
                    <div
                      style={{
                        display: "flex",
                        flexWrap: "wrap",
                        justifyContent: "space-around",
                        margin: "20px 0",
                      }}
                    >
                      {category?.sub_categories?.map((subcategory) => (
                        <div style={{ width: "100%" }}>
                          <Image
                            style={{ cursor: "pointer" }}
                            preview={false}
                            width={150}
                            src={subcategory.image_path}
                          />
                          <Text
                            style={{
                              display: "flex",
                              padding: "0px 0px 20px 0px",
                              justifyContent: "center",
                              width: "100%",
                            }}
                          >
                            {subcategory.name}
                          </Text>
                        </div>
                      ))}
                    </div>
                  </Col>
                </Row>
              </Col>
              <Col lg={6} sm={24}>
                <div
                  style={{
                    display: "flex",
                    flexWrap: "wrap",
                    justifyContent: "space-around",
                    margin: "20px 0",
                  }}
                >
                  {brands.map((brand) => (
                    <div key={brand.id}>
                      <Image
                        preview={false}
                        style={{ margin: "0 0 15px 0", cursor: "pointer" }}
                        width={130}
                        src={brand.logo}
                      />
                    </div>
                  ))}
                </div>
              </Col>
            </Row>
          </Col>
        </SubMenu>
      ))}
    </Menu>
  );

  const userMenu = (
    <Menu onClick={(e) => onClick(e.key.toString())}>
      <Menu.Item key="info">{t`p-info`}</Menu.Item>
      <Menu.Item key="logout">{t`sign-out`}</Menu.Item>
    </Menu>
  );

  const onClick = async (key: string) => {
    switch (key) {
      case "logout":
        confirm({
          title: t`confirm-logout`,
          onOk: () => {
            dispatch(logoutAsync());
          },
          onCancel: () => {},
          centered: true,
        });
        break;

      case "info":
        push("personal-collection");
        break;

      default:
        return null;
    }
  };

  return (
    <>
      <Row
        justify="space-between"
        align="middle"
        style={{ margin: "4px 10px" }}
        wrap={false}
      >
        <Col>
          <Button
            type="primary"
            onClick={openDrawer}
            style={{ height: 35, width: 35 }}
            icon={
              <MenuOutlined style={{ paddingTop: 3, fontSize: "1.3rem" }} />
            }
          />
        </Col>

        <Col>
          <Row align="middle">
            <Link href="/">
              <img src="/assets/izone.png" width={90} />
            </Link>
          </Row>
        </Col>

        <Col>
          <Space align="center" size="large">
            {!pathname.match(/\/cart/) && (
              <Badge
                count={totalNumberOfProducts}
                style={{
                  border: "none",
                  boxShadow: "none",
                  top: 0,
                  left: `${lang === "ar" ? "25px" : 35}`,
                  padding: "0.5px 1px 0 0",
                }}
              >
                <Link href="/cart">
                  <Button
                    className="navbtndog"
                    type="text"
                    size="small"
                    style={{ height: 35, width: 35 }}
                    icon={
                      <ShoppingCartOutlined
                        style={{
                          color: "#fff",
                          fontSize: 35,
                        }}
                      />
                    }
                  />
                </Link>
              </Badge>
            )}

            <Button
              size="small"
              style={{ height: 35, width: 35 }}
              type="primary"
              onClick={showModal}
              icon={
                <SearchOutlined style={{ paddingTop: 3, fontSize: "1.3rem" }} />
              }
            />
          </Space>
        </Col>
      </Row>

      <Drawer
        placement={lang === "ar" ? "right" : "left"}
        closable={false}
        onClose={onClose}
        visible={visible}
        mask={true}
      >
        <Col>
          {user ? (
            <Dropdown overlay={userMenu} arrow={true} placement="bottomCenter">
              <Button
                type="text"
                style={{
                  display: "block",
                  width: "100%",
                  textAlign: "center",
                  borderBottom: "1px solid #989898",
                  paddingBottom: 45,
                  margin: "10px auto 15px !important",
                  color: "#fff",
                }}
              >
                {user.first_name}
              </Button>
            </Dropdown>
          ) : (
            <Link href="/login">
              <a className={styles.drawer_link}>{t`login`}</a>
            </Link>
          )}
        </Col>

        <Dropdown
          overlay={categories_menu}
          arrow={true}
          placement="bottomCenter"
          trigger={["click"]}
          className={styles.drawer_link}
        >
          <Button
            type="text"
            style={{
              display: "block",
              width: "100%",
              textAlign: "center",
              borderBottom: "1px solid #989898",
              paddingBottom: 45,
              margin: "10px auto 15px !important",
              color: "#fff",
            }}
          >
            {t`shop-by-category`}
          </Button>
        </Dropdown>

        <Col>
          <Link href="/cart">
            <a className={styles.drawer_link}>{t`links.cart`}</a>
          </Link>

          <Link href="/">
            <a className={styles.drawer_link}>{t`links.home`}</a>
          </Link>

          <Link href="/brands">
            <a className={styles.drawer_link}>{t`links.brands`}</a>
          </Link>

          <Link href="/contact">
            <a className={styles.drawer_link}>{t`links.contact-us`}</a>
          </Link>

          <Link href="/check-order">
            <a className={styles.drawer_link}>{t`links.check-order`}</a>
          </Link>
        </Col>
        <Col style={{ position: "relative" }} dir="rtl">
          <Select
            onSelect={async (val: string) => {
              setLoading(true);
              await appServices.changeLang({ lang: val as any });
              replace(route, route, { locale: val });
              setLoading(false);
            }}
            defaultValue={lang}
            loading={loading}
            style={{ width: "100%", margin: "10px 0px 80px 0px" }}
          >
            <Option value="ar">عربي</Option>
            <Option value="en">English</Option>
          </Select>
        </Col>
      </Drawer>

      <Modal
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        bodyStyle={{ padding: "50px 20px" }}
      >
        <Searchbar
          searchCategoryHandler={searchCategoryHandler}
          handleSearchChange={handleSearchChange}
          handleSearchSubmit={handleSearchSubmit}
          searchString={searchString}
          styleObj={{
            boxShadow: "0px 0px 8px 4px #33333330",
            borderRadius: 20,
          }}
        />
      </Modal>
    </>
  );
};
