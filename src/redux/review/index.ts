import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { AppThunk, RootState } from '../store';
import { reviewService } from '../../services/review';
import {
  Review,
  Review_I_Req,
} from '../../models/review';
import ApiErrorNotification from '../../utils/ui/notificationService';
import isError from '../../utils/helpers/is-error';
import requestStatus from '../../constants/enums/request-status';

interface ReviewsState {
  status: requestStatus;
  reviews: Review[];
  review?: Review;
}

let initialState: ReviewsState = {
  status: 'no-thing',
  reviews: [],
};

const ReviewSlice = createSlice({
  name: 'Review',
  initialState,
  reducers: {
    setStatus: (state, { payload }: PayloadAction<requestStatus>) => {
      state.status = payload;
    },
    InsertReview: ({ reviews }, { payload }: PayloadAction<Review>) => {
      reviews.push(payload);
    },
   
   
  },
});

const {
  setStatus,
  InsertReview,

} = ReviewSlice.actions;

export const InsertReviewAsync = (req: Review_I_Req): AppThunk => async (
  dispatch
) => {
  dispatch(setStatus('loading'));
  const result = await reviewService.Insert(req);
  if (isError(result)) {
    ApiErrorNotification(result);
    dispatch(setStatus('error'));
  } else {
    dispatch(InsertReview(result.data));
    dispatch(setStatus('data'));
  }
};


export const selectReview = (state: RootState) => state.Review;

export default ReviewSlice.reducer;
