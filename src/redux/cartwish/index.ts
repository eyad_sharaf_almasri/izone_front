import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from '../store';
import { Product } from '../../models';

interface Item {
  product: Product;
 

}

interface CartState {
  wishlist: Item[];

  checkout: { rate: number | null; price: number | null };
  totalPrice: number;
  finalPrice: number;
  quantity: number;
}

let initialState: CartState = {
  wishlist: [],
  checkout: { rate: null, price: null },
  totalPrice: 0,
  finalPrice: 0,
  quantity: 0,
};

const CartSlice = createSlice({
  name: 'CartWish',
  initialState,
  reducers: {


    InsertItemItem: (state, { payload }: PayloadAction<Item>) => {
      state.wishlist.push(payload);
      state.quantity++;
      },
    DeleteItemItem: ( state, { payload }: PayloadAction<number>) => {
        let index =  state.wishlist.findIndex((el) => el.product.id === payload);
        if (index !== -1)  state.wishlist.splice(index, 1);
        state.quantity--;
      },
    EmptyCartItem: (state, _: PayloadAction) => {
      state.wishlist = [];
      state.totalPrice = 0;
    },
    setTotalPriceItem: (state, { payload }: PayloadAction<number>) => {
      state.finalPrice = payload;
    },
  },
});

export const {
  InsertItemItem,
  DeleteItemItem,
  EmptyCartItem,
  setTotalPriceItem,
} = CartSlice.actions;

export const selectWishListItem = (state: RootState) => state.CartWish.wishlist;
export const selectCheckoutItem = (state: RootState) => state.CartWish.checkout;
export const selectQuantityItem = (state: RootState) => state.CartWish.quantity;
export const selectFinalPriceItem = (state: RootState) => state.CartWish.finalPrice;

export default CartSlice.reducer;
